/*
 * Copyright 2022 rgcenteno.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package testgeneric;

/**
 *
 * @author rgcenteno
 */
public class TestGeneric <E extends Comparable, T> implements Comparable<TestGeneric>{
    private E primero;
    private T segundo; 

    public TestGeneric(E primero, T segundo) {
        this.primero = primero;
        this.segundo = segundo;
    }

    @Override
    public int compareTo(TestGeneric t) {
        return primero.compareTo(t.primero);
    }

    @Override
    public String toString() {
        return primero + ": " + segundo;
    }
    
    
    
    public static void main(String[] args) {
        java.util.Set<TestGeneric<String, Integer>> lista = new java.util.TreeSet<>();
        lista.add(new TestGeneric<>("Hola", 3));
        lista.add(new TestGeneric<>("Adios", 13));
        
        //java.util.Set<TestGeneric<java.util.List<Integer>, Integer>> lista2 = new java.util.TreeSet<>();
        System.out.println(lista);
    }
    
}
