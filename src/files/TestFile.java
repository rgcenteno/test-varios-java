/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package files;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rafael González Centeno
 */
public class TestFile {
    
    public static void main(String[] args) {
        File f = new File(Path.of(".") + File.separator + "numero_no_buffer.txt");
        int num = 65;
        try(OutputStream os = new BufferedOutputStream(new FileOutputStream(f)); //Si añadimos true, haremos un append
            DataOutputStream out = new DataOutputStream(os)){
            out.writeInt(num);
            out.writeInt(num + 1);
        } catch (IOException ex) {
            Logger.getLogger(TestFileNoBuffer.class.getName()).log(Level.SEVERE, null, ex);
        }         
        
        File fe = new File(Path.of(".") + File.separator + "numero_no_buffer.txt");
        try(InputStream is = new BufferedInputStream(new FileInputStream(fe));
            DataInputStream di = new DataInputStream(is)   ){
            //Un int son 32 bits = 8 bytes x 4
            while(di.available() >= 4){
                System.out.println(di.readInt());
                System.out.println(di.available());
            }        
        } catch (IOException ex) {
            Logger.getLogger(TestFileNoBuffer.class.getName()).log(Level.SEVERE, null, ex);
        } 
        System.out.println("Finalizado");
    }
}
