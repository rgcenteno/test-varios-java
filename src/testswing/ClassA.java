/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package testswing;

/**
 *
 * @author Rafael González Centeno
 */
public class ClassA {
    int x;

    public ClassA(int x) {
        this.x = x;
    }
    
    public String getTexto(){
        return "Clase A. Valor de x: " + x;
    }
    
    public static void main(String[] args) {
        java.util.List<ClassA> lista = new java.util.LinkedList<>();
        ClassA a = new ClassA(1);
        ClassA a1 = new ClassA(2);
        ClassA a2 = new ClassA(3);
        lista.add(a);
        lista.add(a1);
        lista.add(a2);
        ClassB b = new ClassB(10);
        b.add(a);
        b.add(a1);
        lista.add(b);
        
        ClassB b2 = new ClassB(20);
        b2.add(a2);
        lista.add(b2);
        
        for(ClassA aux : lista){
            System.out.println(aux.getTexto());
        }
                        
    }
}
