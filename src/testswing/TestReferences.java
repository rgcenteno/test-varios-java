/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package testswing;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Rafael González Centeno
 */
public class TestReferences {
    
    
    public static void main(String[] args){
        testReference();
        testList();
    }
    
    private static void testList(){
        System.out.println("lista.delete(3)");
        testList1();
        System.out.println("\nlista.remove(Integer.valueOf(3))");
        testList2();
    }
    
    public static void testReference(){
        System.out.println("test int");
        testInt();
        
        System.out.println("\nTest Integer");
        testInteger();
        
        System.out.println("\nTest String");
        testString();
        
        System.out.println("\nTest StringBuilder");
        testStringBuilder();
        
        System.out.println("\nTest Coche");
        testCoche();
        
    }
    
    private static void testInt(){
        int i = 10;
        System.out.println("i: " + i);
        int j = i;
        j = j + 10;
        System.out.println("i: " + i);
    }
    
    private static void testInteger(){
        Integer i = 10;
        System.out.println("i: " + i);
        Integer j = i;
        j = j + 10;
        System.out.println("i: " + i);
    }
    
    private static void testStringBuilder(){
        StringBuilder sb1 = new StringBuilder("hola");
        System.out.println(sb1);        
        StringBuilder sb2 = sb1;
        sb2.append(" que tal");
        System.out.println(sb1);
    }
    
    private static void testString(){
        String sb1 = "Hola";
        System.out.println(sb1);        
        String sb2 = sb1;
        sb2 = "Hola que tal";
        System.out.println(sb1);
    }
    
    private static void testCoche(){
        Coche c1 = new Coche("SEAT", 100, 150, (byte)3);
        System.out.println(c1);
        Coche c2 = c1;
        c2.setMarca("Marca nueva");
        System.out.println(c1);
    }
    
    private static void testList1(){
        List<Integer> lista = new ArrayList<>(List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 3));
        System.out.println(lista);
        lista.remove(3);
        System.out.println(lista);
    }
    
    private static void testList2(){
        List<Integer> lista = new ArrayList<>(List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 3));
        System.out.println(lista);
        lista.remove(Integer.valueOf(3));
        System.out.println(lista);
    }
}
