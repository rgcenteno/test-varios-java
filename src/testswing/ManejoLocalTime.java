/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package testswing;

import java.time.LocalTime;
import java.time.temporal.ChronoField;
/**
 *
 * @author Rafael González Centeno
 */
public class ManejoLocalTime {
    
    public static void main(String[] args) {
        LocalTime inicio = LocalTime.of(9, 0);
        LocalTime fin = LocalTime.of(10, 40);
        System.out.println("Inicio: " + inicio + " Fin: " + fin);
        long minutos = fin.minusHours(inicio.getHour()).minusMinutes(inicio.getMinute()).get(ChronoField.MINUTE_OF_DAY);
        System.out.println("minutos entre ambos: " + minutos);
    }
}
