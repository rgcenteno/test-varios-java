/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package testswing;

/**
 *
 * @author Rafael González Centeno
 */
public class ClassB extends ClassA{

    private java.util.List<ClassA> hijos;
    public ClassB(int x) {
        super(x);
        hijos = new java.util.LinkedList<>();
    }
    
    public void add(ClassA a){
        this.hijos.add(a);
    }
    
    public String getTexto(){        
        StringBuilder sb = new StringBuilder("Clase B. Valor de x: ");
        sb.append(x).append("\nContiene: \n\t[");
        for(ClassA a : hijos){
            sb.append("\n\t\t").append(a.getTexto());
        }
        sb.append("\n\t]");
        return sb.toString();
    }
}
