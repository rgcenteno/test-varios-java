/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testswing;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author rafa
 */
public class TestSwing {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //MainView main = new MainView();
        //main.setVisible(true);
        System.out.println(coincide("[abc]*", "aabbcc"));
        System.out.println(contiene(" [abc]{3} ","esto es una aaa prueba"));
        System.out.println(!contiene("\\s[1-9][0-9]\\s","esto es una 09 prueba"));
        System.out.println(contiene("[a-zA-Z]{5,10}","esto es una 22 ueba"));
        System.out.println(coincide(".*\\.$", "Esta frase acaba en punto."));
        System.out.println(reemplazar("[a-zA-Z]{5,10}","esto es una 22 prueba"));        
    }
    
    public static boolean coincide(String patron, String cadena){
        Pattern pat = Pattern.compile(patron);
        Matcher mat = pat.matcher(cadena);                                                                           
        return mat.matches();
    }    
    
    public static boolean contiene(String patron, String cadena){
        Pattern pat = Pattern.compile(patron);
        Matcher mat = pat.matcher(cadena);                                                                           
        return mat.find();
    }  
    
    public static String reemplazar(String patron, String cadena){
        Pattern pat = Pattern.compile(patron);
        Matcher mat = pat.matcher(cadena);                      
        return mat.replaceAll("****");
    }
}
