/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package testswing;

/**
 *
 * @author Rafael González Centeno
 */
public class Coche {
    // atributos:
    private String marca;
    private double velMax;
    private int potencia;
    private byte posicion;

    public Coche(String marca, double velMax, int potencia, byte posicion) {
        Coche.checkPosicion(potencia);
        Coche.checkPotencia(potencia);
        Coche.checkVelocidadMaxima(velMax);
        this.marca = marca;
        this.velMax = velMax;
        this.potencia = potencia;
        this.posicion = posicion;
    }
    
    private static void checkVelocidadMaxima(double vel){
        if(vel <= 0){
            throw new IllegalArgumentException("La velocidad debe tener un valor mayor que cero");
        }
    }
    
    private static void checkPotencia(int potencia){
        if(potencia <= 0){
            throw new IllegalArgumentException("La potencia debe tener un valor mayor que cero");
        }
    }
    
    private static void checkPosicion(int posicion){
        if(posicion <= 0){
            throw new IllegalArgumentException("La posicion debe tener un valor mayor que cero");
        }
    }
    //Métodos get que permiten la consulta 
    //del valor de los atributos
    public String getMarca() {
        return marca;
    }
    public double getVelMax() {
        return velMax;
    }
    public int getPotencia() {
        return potencia;
    }
    public byte getPosicion() {
        return posicion;
    }

    //Métodos set que permiten la modificación 
    //del valor de los atributos
    public void setMarca(String marca) {
        this.marca = marca;
    }
    public void setVelMax(double velMax) {
        Coche.checkVelocidadMaxima(velMax);
        this.velMax = velMax;
    }
    public void setPotencia(int potencia) {
        Coche.checkPotencia(potencia);
        this.potencia = potencia;
    }  
    public void setPosicion(byte posicion) {
        Coche.checkPosicion(potencia);
        this.posicion = posicion;
    }  

    @Override
    public String toString() {
        return "Marca: " + this.marca + "\nVelMax: " + this.velMax + "\nPotencia: " + this.potencia + "\nPosición: " + this.posicion;
    }
    
    
}   

