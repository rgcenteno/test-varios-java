/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package clonetest;

/**
 *
 * @author Rafael González Centeno
 */
public class User implements Cloneable{
     private String firstName;
    private String lastName;
    private Address address;

    public User(String firstName, String lastName, Address address) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "User{" + "firstName=" + firstName + ", lastName=" + lastName + ", address=" + address + '}';
    }
    
    

    @Override
    protected Object clone() throws CloneNotSupportedException {
        User user = (User)super.clone(); 
        user.address = (Address)this.address.clone();
        return user;
    }
    
    public static void main(String[] args) throws CloneNotSupportedException {
        Address address = new Address("Downing St 10", "London", "England");
        User pm = new User("Prime", "Minister", address);
        User deepCopy = (User) pm.clone();
        System.out.println(pm);
        System.out.println(deepCopy);

        address.setCountry("Great Britain");
        System.out.println("Tras edición");
        System.out.println(pm);
        System.out.println(deepCopy);
    }
    
}
