/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package multithread;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rafael González Centeno
 */
public class RunnableCampoComun implements Runnable{

    private final int id;
    private int ejecuciones;
    private java.util.Random random;
    private ClaseContenedora contenedor;
    
    private static final int MAX = 100;
    
    public RunnableCampoComun(int id, ClaseContenedora c) {
        this.id = id;
        ejecuciones = 0;
        this.contenedor = c;
        random = new java.util.Random();
    }

    @Override
    public void run() {
        System.out.println("************* Hilo " + id + " comienza ***************");
        do{
            ejecuciones++;
            contenedor.sumarContador();
            System.out.println("Soy el hilo " + id + ". Contenedor: " + contenedor.getContador());
            try {
                Thread.sleep(random.nextInt(100));
            } catch (InterruptedException ex) {
                Logger.getLogger(RunnableTest.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        while(ejecuciones < MAX);
        System.out.println("FINAL HILO: " + id);
    }
    
}
