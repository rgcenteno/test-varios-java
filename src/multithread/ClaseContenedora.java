/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package multithread;

/**
 *
 * @author Rafael González Centeno
 */
public class ClaseContenedora {
    private int contador;

    public ClaseContenedora() {
        this.contador = 0;
    }
    
    public void sumarContador(){
        contador++;
    }

    public int getContador() {
        return contador;
    }    
}
