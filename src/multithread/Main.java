/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package multithread;

import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rafael González Centeno
 */
public class Main {
    
    private static int max = 10;
    
    public static void main(String[] args) throws InterruptedException {
        test11();
    }
    
    private static void test1() throws InterruptedException{
        Thread[] hilos = new Thread[max];
        for (int i = 0; i < hilos.length; i++) {
            hilos[i] = new Thread(new RunnableTest(i+1));
            hilos[i].start();
            
        }
        for (int i = 0; i < hilos.length; i++) {
            hilos[i].join();
        }
        System.out.println("Todos finalizados");
    }
    
    private static void test2() throws InterruptedException{
        ClaseContenedora c = new ClaseContenedora();
        Thread[] hilos = new Thread[max];
        for (int i = 0; i < hilos.length; i++) {
            hilos[i] = new Thread(new RunnableCampoComun(i+1, c));
            hilos[i].start();
            
        }
        for (int i = 0; i < hilos.length; i++) {
            hilos[i].join();
        }
        System.out.println("Todos finalizados " + c.getContador());
    }
    
    private static void test11(){
        try {            
            ExecutorService executor = Executors.newFixedThreadPool(5);
            for (int i = 0; i < max; i++) {                        
                executor.execute(new RunnableTest(i+1));
            }
            //No permitimos que añadan más elementos
            executor.shutdown();
            executor.awaitTermination(1, TimeUnit.HOURS);
            System.out.println("Todos finalizados ");
        } catch (InterruptedException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private static void test12(){
        try {
            ClaseContenedora c = new ClaseContenedora();
            ExecutorService executor = Executors.newFixedThreadPool(5);
            for (int i = 0; i < max; i++) {                        
                executor.execute(new RunnableCampoComun(i+1, c));
            }
            //No permitimos que añadan más elementos
            executor.shutdown();
            executor.awaitTermination(1, TimeUnit.HOURS);
            System.out.println("Todos finalizados " + c.getContador());
        } catch (InterruptedException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
