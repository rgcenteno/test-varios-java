/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package abstracty;

/**
 *
 * @author Rafael González Centeno
 */
@FunctionalInterface
public interface ClaseInterfaz<T> {
    public int DATO = 3;
    public static int getData(){
        return 1;
    }
    
    public int compareTo(T t);
}
