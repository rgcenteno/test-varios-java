/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package filestext;

import files.TestFileNoBuffer;
import java.io.File;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import java.io.Reader;
import java.io.Writer;
import java.nio.file.Path;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rafael González Centeno
 */
public class TextFileNoBuffer {
    public static void main(String[] args) {
        File f = new File(Path.of(".") + File.separator + "text_no_buffer.txt");
        try(Writer os = new FileWriter(f)){ //Prueba el mismo código con éste constructor new FileWriter(f, true)
            os.append("mi texto\n");
            os.append("otro texto\n");
        } catch (IOException ex) {
            Logger.getLogger(TestFileNoBuffer.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        File fe = new File(Path.of(".") + File.separator + "text_no_buffer.txt");
        try(Reader reader = new FileReader(fe)){            
            int n = reader.read();
            while(n != -1){
                System.out.print((char)n);
                n = reader.read();
            }
        } catch (IOException ex) {
            Logger.getLogger(TestFileNoBuffer.class.getName()).log(Level.SEVERE, null, ex);
        } 
        System.out.println("Finalizado");
    }
}
