/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package filestext;

import files.TestFileNoBuffer;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.nio.file.Path;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rafael González Centeno
 */
public class TextFile {
    public static void main(String[] args) {
        File f = new File(Path.of(".") + File.separator + "text.txt");
        //Usamos BufferedWriter porque además trae el método newLine
        try(BufferedWriter br = new BufferedWriter(new FileWriter(f))){ //Prueba el mismo código con éste constructor new FileWriter(f, true)
            br.write("Mi texto");
            br.newLine();
            br.append("otro texto");
            br.newLine();
        } catch (IOException ex) {
            Logger.getLogger(TestFileNoBuffer.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        File fe = new File(Path.of(".") + File.separator + "text.txt");
        try(BufferedReader reader = new BufferedReader(new FileReader(fe))){            
            String s = reader.readLine();
            while(s != null){
                System.out.println(s);
                s = reader.readLine();
            }
        } catch (IOException ex) {
            Logger.getLogger(TestFileNoBuffer.class.getName()).log(Level.SEVERE, null, ex);
        } 
        System.out.println("Finalizado");
    }
}
